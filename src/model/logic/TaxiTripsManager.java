package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.data_structures.Area;
import model.data_structures.HashTable;
import model.data_structures.Iterator;
import model.data_structures.Key;
import model.data_structures.Queue;
import model.data_structures.Value;
import model.vo.Compania;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
    private Queue<Compania> companias;
    
	public boolean cargarSistema(String arch, String fechaI, String fechaF) throws ParseException {
		// TODO Auto-generated method stub
		boolean c = false;
		JsonParser jp = new JsonParser();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Date in = formato.parse(fechaI);
		Date fnl = formato.parse(fechaF);
		try
		{
			JsonArray ja = (JsonArray) jp.parse(new FileReader(arch));
			for (int i = 0; ja !=null && i < ja.size(); i++)	
			{
				JsonObject jo = (JsonObject) ja.get(i);

				String[] datosServicios = new String[5];
				String nombre = "NaN";
				if ( jo.get("company") != null )
				{ nombre = jo.get("company").getAsString();}
				Compania compania = new Compania(nombre);
				if(!existeCompa�ia(nombre))companias.enqueue(compania);


				String taxiId = "";
				if ( jo.get("taxi_id") != null )
				{ taxiId = jo.get("taxi_id").getAsString(); }


				if ( jo.get("trip_id") != null )datosServicios[0] =jo.get("tripId").getAsString();


				if ( jo.get("trip_miles") != null )datosServicios[1] =jo.get("trip_miles").getAsString();


				if ( jo.get("trip_total") != null )datosServicios[2] =jo.get("trip_total").getAsString();

				if(jo.get("pickup_community_area")!=null)datosServicios[3] =jo.get("pickup_community_area").getAsString();

				if(jo.get("trip_start_timestamp")!=null)datosServicios[4] =jo.get("trip_start_timestamp").getAsString();

				if(jo.get("trip_end_timestamp")!=null)datosServicios[5] =jo.get("trip_end_timestamp").getAsString();


				compania.addTaxi(taxiId,datosServicios);







				System.out.println("------------------");
				System.out.println(jo);
				if(jo != null)
				{
					c = true;
				}


			}	


			JsonReader jr = new JsonReader(new FileReader(arch));
			Gson gs = new GsonBuilder().create();

			jr.beginArray();	
			jr.close();

		}	

		catch(IOException e)
		{
			e.printStackTrace();

		}

		return c;
	}

	
	public Queue<Service> serviciosArea(Area area){
		
		Queue<Service> servicios = new Queue<Service>();
		
		for(int i=0; i<arregloCompanias().length;i++){
			
		Queue<Service> areas = arregloCompanias()[i].serviciosArea(area);
		
		for(int j=0; j<areas.size();j++){
			
			servicios.enqueue(areas.get(i));
		}
	
		}
		
		return servicios;
		
	}


	public boolean existeCompa�ia(String name){

		return false;
	}
	public void ordenarHeap(Taxi taxis[])
	{
		

	}
	public void taxisHeap(Taxi taxis[], int a, int i)
	{
		
	}
	
	public void companiasHeap(Compania companias[], int a, int i)
	{
	
	}

	public Compania[] arregloCompanias(){
		Iterator<Compania> iterador = companias.iterator();
		int i = 0;
		Compania[] companias1 = new Compania[companias.size()];
		boolean encontro = false;
		while(iterador.hasNext()){
			Compania actual = iterador.Next();
			companias1[i]= actual;
			i++;
		}
		return companias1;
	}


	@Override
	public void prioridadCompanias(Compania[] companias) {
		// TODO Auto-generated method stub
		
	}
	
	public HashTable<Key, Value> agruparServiciosDistancia(Queue pServicios)
	{
		Key<int[][]> x;
		
		return null;
		
	}


}

