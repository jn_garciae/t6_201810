package model.data_structures;

public class Area {

	private int area;
	
	public Area(int i){
		
		area = i;
	}
	
	public int area(){
		return area;
	}
}
