package model.data_structures;

public interface Iterator<E>{

	public boolean hasNext();

	public E Next();

	

}
