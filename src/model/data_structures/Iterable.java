package model.data_structures;

public interface Iterable<T> {

	public Iterator iterator();
}
