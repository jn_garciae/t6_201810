package model.data_structures;

import java.util.NoSuchElementException;

public class ListIterator<E> implements Iterator<E>{




	private Node<E> proximo;
	private E item;


	public ListIterator(Node<E> r){

		proximo = r;

		item = proximo.item;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return proximo!=null ;
	}

	@Override
	public E Next() {
		// TODO Auto-generated method stub
		if (!hasNext()){
			throw new NoSuchElementException("No hay proximo");
		}


		E item2 = item;
		proximo = proximo.next; 
		return item2;

	}



}

