package model.data_structures;
import java.util.Scanner;
import model.data_structures.Key;
public class HashTable <Key, Value> implements IHashTable<Key,Value>{
	
	    private static final int INIT_CAPACITY = 4;

	     int n;                                // number of key-value pairs
	    int m;                                // hash table size
	    SequentialSearchST<Key, Value>[] st;  // array of linked-list symbol tables


	    
	    public HashTable() {
	        this(INIT_CAPACITY);
	    } 

	    

	    public HashTable(int m) {
	        this.m = m;
	        st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[m];
	        for (int i = 0; i < m; i++)
	            st[i] = new SequentialSearchST<Key, Value>();
	    } 

	    // resize the hash table to have the given number of chains,
	    // rehashing all of the key
	    public void resize(int chains) {
	        HashTable<Key, Value> temp = new HashTable<Key, Value>(chains);
	        for (int i = 0; i < m; i++) {
	            for (Key key : st[i].keys()) {
	                temp.put(key, st[i].get(key));
	            }
	        }
	        this.m  = temp.m;
	        this.n  = temp.n;
	        this.st = temp.st;
	    }

	     
	    public int hash(Key key) {
	        return (key.hashCode() & 0x7fffffff) % m;
	    } 

	 
	    public int size() {
	        return n;
	    } 

	    
	    public boolean isEmpty() {
	        return size() == 0;
	    }

	    
	    public boolean contains(Key key) {
	        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
	        return get(key) != null;
	    } 

	   
	    public Value get(Key key) {
	        if (key == null) throw new IllegalArgumentException("argument to get() is null");
	        int i = hash(key);
	        return st[i].get(key);
	    } 

	   
	    public void put(Key key, Value val) {
	        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
	        if (val == null) {
	            delete(key);
	            return;
	        }

	       
	        if (n >= 10*m) resize(2*m);

	        int i = hash(key);
	        if (!st[i].contains(key)) n++;
	        st[i].put(key, val);
	    } 

	    
	    public void delete(Key key) {
	        if (key == null) throw new IllegalArgumentException("argument to delete() is null");

	        int i = hash(key);
	        if (st[i].contains(key)) n--;
	        st[i].delete(key);

	        
	        if (m > INIT_CAPACITY && n <= 2*m) resize(m/2);
	    } 

	    public Iterator<Key> keys() {
	        Queue<Key> queue = new Queue<Key>();
	        for (int i = 0; i < m; i++) {
	            for (Key key : st[i].keys()	){
	                queue.enqueue(key);
	        }
	            
	        }
	        return queue.iterator();
	    } 


	    
	    public static void main(String[] args) { 
	        SeparateChainingHashST<String, Integer> st = new SeparateChainingHashST<String, Integer>();
	        for (int i = 0; !StdIn.isEmpty(); i++) {
	            String key = StdIn.readString();
	            st.put(key, i);
	        }

	        
	        for (String s : st.keys()) 
	            StdOut.println(s + " " + st.get(s)); 

	    }

�



	
}
