package model.data_structures;

import java.util.NoSuchElementException;

public class Queue<T> implements IQueue<T>, Iterable<T>{

	Node<T> first;

	Node<T> last;
	
	int longitud;
	
	
	public T get(T item){
		
		T retornar = null;
		
		while(iterator().hasNext()){
			
			T actual = iterator().Next();
			
			if(actual.equals(item)){
				retornar = actual;
			}
				
		}
		return retornar;
	}
	
	public T get(int i){
		
		Node<T> frs = first;
		T item = null;
		boolean encontro = false;
		while(frs!=null||encontro){
			if(frs.num==i){
				item = frs.item;
				encontro = true;
			}
			
			frs = frs.next;
		}
		return item;
	}
	@Override
	public void enqueue(T item) {
		Node<T> oldlast = last;
		last = new Node<T>(item);
		last.next = null;
		if (isEmpty()){
			first = last;
			first.num = 0;
		}
		else{
			oldlast.next = last;
			last.num = oldlast.num+1;
		}
		longitud++;
		

	}

	@Override
	public T dequeue() {
		int x = -1;
		if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        T item = first.item;
        first = first.next;
        Node<T> frs = first;
        while(frs!=null){
        	first.num= x+1;
        	frs = frs.next;
        }
        longitud--;
        if (isEmpty()) last = null;   
        return item;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first==null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return longitud;
	}

	@Override
	public Iterator<T> iterator() {
		Iterator<T> listIterator = new ListIterator<T>(first);
		return listIterator;
	}

	@Override
	public Queue<T> merge(int[] arr, int l, int m, int r) {
		// TODO Auto-generated method stub
		return null;
	}

}
