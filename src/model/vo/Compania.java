package model.vo;

import model.data_structures.Area;
import model.data_structures.Iterator;
import model.data_structures.Queue;

public class Compania 

{
	private int totalServicios;

	private String nombre;

	private Queue<Taxi> taxisInscritos;	


	public Compania(String name){
		
	nombre = name;	
	}
	
	
	
	public int darTotalServicios()
	{
		return totalServicios;
	}
	public int compareTo(Compania p)
	{
		if(totalServicios < p.darTotalServicios())
		{
			return 0;
		}
		else 
		{
			return 1;
		}
	
	}
	
	public String name(){
		
		return nombre;
	}
	public  void addTaxi(String identificador, String[] datosServicios){
		
		Taxi taxi1 = new Taxi(identificador);
		
		Iterator<Taxi> iterador = taxisInscritos.iterator();
		boolean encontro = false;
		while(iterador.hasNext()&&!encontro){
			Taxi actual = iterador.Next();
			
			if(actual.getTaxiId()==Integer.parseInt(identificador)){
				encontro = true;
				actual.addService(datosServicios);
				
			}
			
			else{
				taxisInscritos.enqueue(taxi1);
				taxi1.addService(datosServicios);
			}
			} 
	}
	
	public Taxi[] arregloTaxi(){
		Iterator<Taxi> iterador = taxisInscritos.iterator();
		int i = 0;
		Taxi[] serv = new Taxi[taxisInscritos.size()];
		while(iterador.hasNext()){
		Taxi actual = iterador.Next();
			serv[i]= actual;
			i++;
	}
   return serv;
}
	

	public Queue<Service> serviciosArea(Area area){
		
		Queue<Service> areas = new Queue<Service>();
		for(int i=0; i<arregloTaxi().length;i++){
			Queue<Service> temporal = arregloTaxi()[i].serviciosArea(area);
			
			for(int j=0; j<temporal.size();j++){
				
				areas.enqueue(temporal.get(i));
			}
				
			}
		
		
		return areas;
	}
	
	
}
