package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Iterator;
import model.data_structures.Area;
import model.data_structures.HashTable;
/**
 * Representation of a taxi object
 */

import  model.data_structures.Queue;

public class Taxi implements Comparable<Taxi>{

	
	Queue<Service> servicios;
	private int id;
	
	private HashTable<Area,Service> service;
	
	
	public Taxi(String identificador){
		
		id = Integer.parseInt(identificador);
		
	}
	/**
	 * @return id - taxi_id
	 */
	public int getTaxiId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return "company";
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		if(id <= o.getTaxiId())
		{
			return 0;
		}
		else
			return 1;
	}	
	
	public void servicesInOrder(){
		
		int[] num = new int[2]; 
		
	 
				
			}
	
	public void addService(String[] datosServicios) {
		Service servicios1 = new Service(datosServicios,id);
		Area area = new Area(Integer.parseInt(datosServicios[3]));
		service.put(area, servicios1);
	}
	
	
			
	public Queue<Service> serviciosArea(Area area) {
		
		Iterator<Area> iterador =  service.keys();
		Queue<Service> serviciosArea = new Queue<Service>();
		
		while(iterador.hasNext()){
			
			Area actual = iterador.Next();
			
			if(actual.area()==area.area()){
				
				serviciosArea.enqueue(service.get(actual));
			}
		}
		
		return serviciosArea;
		
	}	
}
		
		

