package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.data_structures.Area;

/**
 * Representation of a Service object
 */



public class Service implements Comparable<Service> {

	private String id, taxiId;
	
	private double miles;

	private Double total;
	
	private Area area;
	
	private Date time;
	
	public Service(String[] datos, int idT){
		id = datos[0];
		taxiId = ""+idT;
		miles = Double.parseDouble(datos[1]);
		total = Double.parseDouble(datos[2]);
		area = new Area(Integer.parseInt(datos[3]));
		time = convertirDate(datos[4]);
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return total;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public Date getTripStartTime() {
		// TODO Auto-generated method stub
		return time;
	}

	
	public Area getTripArea(){
		return area;
	}
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public Date convertirDate(String fecha) {
		
		SimpleDateFormat fecha1 = new SimpleDateFormat("yyyy - dd -MM T HH:mm:ss");
	    Date retornar = null;
		try {
			retornar = fecha1.parse(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return retornar;
	
	}
}
