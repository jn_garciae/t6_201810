package api;

import model.vo.Taxi;

import java.text.ParseException;

import model.data_structures.Iterator;
import model.vo.Compania;
import model.vo.Service;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services 
	 * @param taxiId - taxiId of interest 
	 * @throws ParseException 
	 */
	public boolean cargarSistema(String arch, String fechaI, String fechaF) throws ParseException;
	
	/**
	 * 
	 * @param taxis
	 */
	public void ordenarHeap(Taxi taxis[]);
  
	public void prioridadCompanias( Compania companias[]);
	public Compania[] arregloCompanias();

}
